module.exports = {
  transpileDependencies: [
    'vuetify'
  ],

  pwa: {
    workboxOptions: {
      skipWaiting: true,
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'ja',
      fallbackLocale: 'ja',
      localeDir: 'locales',
      enableInSFC: true,
      enableBridge: false
    }
  }
};
